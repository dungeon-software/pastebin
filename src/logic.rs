use crate::templates::ErrorTemplate;
use crate::utils::generate_random_path;
use actix_web::HttpResponse;
use askama::Template;
use chrono::serde::ts_seconds_option;
use chrono::{DateTime, Utc};
use deadpool_redis::Pool;
use log::{error, info};
use redis::{AsyncCommands, RedisResult};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Note {
    pub title: String,
    pub text: String,
    pub password: Option<String>,
    #[serde(with = "ts_seconds_option")]
    pub expiration: Option<DateTime<Utc>>,
    pub reads_left: Option<u32>,
}

impl Note {
    pub fn is_expired(&self) -> bool {
        match &self.expiration {
            None => false,
            Some(date) => date.lt(&Utc::now()),
        }
    }

    pub fn is_editable(&self) -> bool {
        match self.reads_left {
            None => self.password.is_some(),
            Some(reads_left) => self.password.is_some() && reads_left >= 1,
        }
    }

    pub fn passwords_dont_match(&self, password: &str) -> bool {
        match &self.password {
            None => false,
            Some(note_password) => note_password != password,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EditedNote {
    pub title: String,
    pub text: String,
    pub password: String,
}

pub enum NoteInsertResult {
    Ok,
    Err(HttpResponse),
}

pub enum NoteQueryResult {
    Ok(Note),
    Err(HttpResponse),
}

pub enum NotePathResult {
    Ok(String),
    Err(HttpResponse),
}

pub struct AppState {
    pool: Pool,
}

impl AppState {
    pub fn new(pool: Pool) -> Self {
        Self { pool }
    }

    pub async fn add_note(&self, note: Note) -> NotePathResult {
        let Ok(mut connection) = self.pool.get().await else {
            error!("Failed acquiring connection to Redis!");
            let error_template = ErrorTemplate {description: "Failed initializing connection!"};
            return NotePathResult::Err(HttpResponse::InternalServerError().body(error_template.render().unwrap()));
        };

        loop {
            let path = generate_random_path();

            let Ok(path_exists) = connection.exists(path.clone()).await else {
                let error_template = ErrorTemplate {description: "Failed accessing data"};
                return NotePathResult::Err(HttpResponse::InternalServerError().body(error_template.render().unwrap()));
            };

            if path_exists {
                continue;
            }

            drop(connection);
            return match self.insert_note(&path, note.clone()).await {
                NoteInsertResult::Ok => NotePathResult::Ok(path),
                NoteInsertResult::Err(response) => NotePathResult::Err(response),
            };
        }
    }

    pub async fn insert_note(&self, path: &str, note: Note) -> NoteInsertResult {
        let Ok(mut connection) = self.pool.get().await else {
            error!("Failed acquiring connection to Redis!");
            let error_template = ErrorTemplate {description: "Failed initializing connection!"};
            return NoteInsertResult::Err(HttpResponse::InternalServerError().body(error_template.render().unwrap()));
        };

        let serialized_note =
            serde_json::to_string(&note).expect("Serialize not implemented for Note");

        if connection
            .set::<String, String, String>(path.to_string(), serialized_note)
            .await
            .is_err()
        {
            error!("Failed inserting note with ID: {:?}", path);
            let error_template = ErrorTemplate {
                description: "Failed entering data",
            };
            return NoteInsertResult::Err(
                HttpResponse::InternalServerError().body(error_template.render().unwrap()),
            );
        };

        info!("Note: {:?} posted on path: {}", note, path);
        NoteInsertResult::Ok
    }

    pub async fn get_note(&self, path: &str) -> NoteQueryResult {
        let Ok(mut connection) = self.pool.get().await else {
            error!("Failed acquiring connection to Redis!");
            let error_template = ErrorTemplate {description: "Failed initializing connection!"};
            return NoteQueryResult::Err(HttpResponse::InternalServerError().body(error_template.render().unwrap()));
        };

        let Ok(data): RedisResult<String> = connection.get(path).await else {
            error!("Failed retrieving note with ID: {:?}", path);
            let error_template = ErrorTemplate {description: "Note not found"};
            return NoteQueryResult::Err(HttpResponse::NotFound().body(error_template.render().unwrap()))
        };

        let Ok(note) = serde_json::from_str::<Note>(&data) else {
            error!("Failed serializing note data: {:?} on path {:?}", data, path);
            let error_template = ErrorTemplate {description: "Note data corrupted"};
            return NoteQueryResult::Err(HttpResponse::InternalServerError().body(error_template.render().unwrap()))
        };

        info!("Retrieved note with ID: {:?}", path);
        NoteQueryResult::Ok(note)
    }

    pub async fn remove_note(&self, path: &str) {
        let Ok(mut connection) = self.pool.get().await else {
            error!("Failed acquiring connection to Redis!");
            return;
        };

        let _ = connection.del::<String, i32>(path.to_string()).await;
        info!("Removed note with ID: {}", path);
    }

    pub async fn decrease_count_to_burn(&self, path: &str, note: &mut Note) {
        let Some(reads_left) = note.reads_left.as_mut() else {
            return;
        };

        *reads_left -= 1;

        if *reads_left == 0 {
            info!("Burning note with ID: {}", path);
            self.remove_note(path).await;
        } else {
            info!("{} reads left on node ID: {}", reads_left, path);
            self.insert_note(path, note.clone()).await;
        }
    }

    pub async fn edit_note(
        &self,
        path: &str,
        mut original_note: Note,
        new_title: String,
        new_text: String,
    ) {
        original_note.title = new_title.clone();
        original_note.text = new_text.clone();

        self.insert_note(path, original_note.clone()).await;

        info!(
            "Edited {:?} with ID: {:?} to title: {} and text: {}",
            original_note, path, new_title, new_text
        );
    }
}
