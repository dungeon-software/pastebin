use rand::distributions::Alphanumeric;
use rand::Rng;

pub fn generate_random_path() -> String {
    rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(20)
        .map(char::from)
        .collect()
}
