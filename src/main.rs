use crate::api::{get_note, index, post_existing_note, post_note};
use actix_web::{web, App, HttpServer};
use clap::Parser;
use deadpool_redis::{Config, Runtime};
use log::info;
use logic::AppState;
use simple_logger::SimpleLogger;
use std::fmt::Debug;

mod api;
mod logic;
mod templates;
mod utils;

/// Simple Pastebin application
#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// Port on which the HTTP server will run
    #[arg(short, long, default_value = "8080")]
    port: u16,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    SimpleLogger::new().init().unwrap();

    let config = Config::from_url("redis://127.0.0.1:6379/");
    let pool = config.create_pool(Some(Runtime::Tokio1)).unwrap();
    let data = web::Data::new(AppState::new(pool));

    let args = Args::parse();
    info!("Starting server on port {}", args.port);
    HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .service(index)
            .service(post_note)
            .service(get_note)
            .service(post_existing_note)
            .service(actix_files::Files::new("/frontend", "./frontend"))
    })
    .bind(("127.0.0.1", args.port))?
    .run()
    .await
}
