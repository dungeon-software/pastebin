use askama::Template;

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate;

#[derive(Template)]
#[template(path = "detail.html")]
pub struct EditableNoteTemplate<'a> {
    pub title: &'a str,
    pub text: &'a str,
}

#[derive(Template)]
#[template(path = "detail-readonly.html")]
pub struct ReadOnlyNoteTemplate<'a> {
    pub title: &'a str,
    pub text: &'a str,
}

#[derive(Template)]
#[template(path = "error.html")]
pub struct ErrorTemplate<'a> {
    pub description: &'a str,
}
