use crate::logic::{EditedNote, Note, NotePathResult, NoteQueryResult};
use crate::templates::{EditableNoteTemplate, ErrorTemplate, IndexTemplate, ReadOnlyNoteTemplate};
use crate::AppState;
use actix_web::{get, post, web, HttpResponse, Responder};
use askama::Template;
use log::info;

#[get("/")]
pub async fn index() -> impl Responder {
    info!("Rendering index page");
    let content = IndexTemplate {};

    HttpResponse::Ok().body(content.render().unwrap())
}

#[post("/")]
pub async fn post_note(data: web::Data<AppState>, note: web::Json<Note>) -> impl Responder {
    match data.add_note(note.clone()).await {
        NotePathResult::Ok(path) => HttpResponse::Ok().json(path),
        NotePathResult::Err(response) => response,
    }
}

#[get("/{path}")]
pub async fn get_note(path: web::Path<String>, data: web::Data<AppState>) -> impl Responder {
    let path = &path.into_inner();

    let mut note = match data.get_note(path).await {
        NoteQueryResult::Ok(note) => note,
        NoteQueryResult::Err(response) => return response,
    };

    if note.is_expired() {
        info!("Note with ID: {} is expired", path);
        data.remove_note(path).await;

        let template = ErrorTemplate {
            description: "Note not found",
        };
        return HttpResponse::NotFound().body(template.render().unwrap());
    }

    data.decrease_count_to_burn(path, &mut note).await;

    let content = if note.is_editable() {
        EditableNoteTemplate {
            title: &note.title,
            text: &note.text,
        }
        .render()
    } else {
        ReadOnlyNoteTemplate {
            title: &note.title,
            text: &note.text,
        }
        .render()
    };

    HttpResponse::Ok().body(content.unwrap())
}

#[post("/{path}")]
pub async fn post_existing_note(
    path: web::Path<String>,
    data: web::Data<AppState>,
    edited_note: web::Json<EditedNote>,
) -> impl Responder {
    let path = &path.into_inner();

    let note = match data.get_note(path).await {
        NoteQueryResult::Ok(note) => note,
        NoteQueryResult::Err(response) => return response,
    };

    if note.passwords_dont_match(&edited_note.password) {
        info!(
            "Invalid password \"{}\" provided for note with ID: {}",
            edited_note.password, path,
        );
        let error_template = ErrorTemplate {
            description: "Invalid password",
        };
        return HttpResponse::BadRequest().body(error_template.render().unwrap());
    }

    data.edit_note(
        path,
        note,
        edited_note.title.clone(),
        edited_note.text.clone(),
    )
    .await;
    HttpResponse::Ok().json("OK")
}
