function submit()
{
    const title = document.querySelector("#title-text").value;
    const text = document.querySelector("#content-text").value;
    let password = document.querySelector("#password-text").value;
    let time = document.querySelector("#expiration").value;
    let burns = document.querySelector("#burns").value;

    if (title.length === 0) {
        alert("Note needs a title!");
        return;
    }

    if (text.length === 0) {
        alert("Note has no text!");
        return;
    }

    if (password.length === 0) {
        password = null;
    }

    if (time.length === 0) {
        time = null;
    } else {
        time = Math.floor(new Date(time).getTime() / 1000);
        let current_time = Math.floor(Date.now() / 1000);

        if (current_time >= time) {
            alert("Invalid expiration time, selected date is older than current time");
            return;
        }
    }

    if (burns.length === "") {
        burns = null;
    } else {
        burns = parseInt(burns.trim(), 10);

        if (burns <= 0) {
            alert("Reads until burned cannot be 0 or a negative number!");
            return;
        }
    }


    const xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            let response = xhr.responseText;
            response = response.replace(/['"]+/g, '')
            window.location.replace(window.location + response)
        }
    }
    xhr.open("POST", "", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        "title": title,
        "text": text,
        "password": password,
        "expiration": time,
        "reads_left": burns
    }));

    alert("Submitted your " + title + " note!");
}

function edit() {
    const title = document.querySelector("#title-text").value;
    const text = document.querySelector("#content-text").value;
    const password = document.querySelector("#password-text").value;

    if (title.length === 0) {
            alert("Note needs a title!");
            return;
        }

        if (text.length === 0) {
            alert("Note has no text!");
            return;
        }

        if (password.length === 0) {
            alert("You need to enter a password to unlock editing!");
            return;
        }

        const xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                let response = xhr.responseText;
                response = response.replace(/['"]+/g, '')

                if (response === "OK") {
                    alert("Note successfully edited!");
                    location.reload();
                } else {
                    alert("Invalid password!");
                }
            }
        }
        xhr.open("POST", "", true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(JSON.stringify({
            "title": title,
            "text": text,
            "password": password
        }));
}