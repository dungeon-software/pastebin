FROM alpine:latest AS build

RUN apk update && apk upgrade --no-cache && apk add cargo

WORKDIR /app

COPY . .

RUN ["cargo", "build", "--release"]

FROM alpine:latest AS release

WORKDIR /app

COPY frontend /app/frontend
COPY templates /app/templates
COPY --from=build /app/target/release/pastebin /app/pastebin

ENTRYPOINT ["./pastebin"]